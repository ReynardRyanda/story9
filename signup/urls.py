from django.urls import path
from .views import *

app_name = 'signup'

urlpatterns = [
    path('', content, name='content'),
    path('signup/', signup, name='signup'),
    path('add_user/', add_user, name='add_user'),
]
