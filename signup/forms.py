from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
# from .models import Custom_User


class SignUpForm(forms.Form):
    name = forms.CharField(widget=forms.TextInput(attrs={
        'class': "form-control",
        'id': "username",
        'placeholder': "Enter username"
    }), min_length=1)
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'class': "form-control",
        'id': "password",
        'placeholder': "Password"
    }), min_length=8, max_length=32)
