from django.shortcuts import render, redirect
from .forms import *
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
# from .models import Custom_User
# from django.contrib.sessions.backends.db import s


# Create your views here.
def signup(request):
    context = {
        'form': SignUpForm()
    }
    return render(request, 'signup.html', context)


def add_user(request):
    if SignUpForm(request.POST).is_valid():
        if not User.objects.filter(username=request.POST['name']).exists():
            user = User.objects.create_user(
                request.POST['name'], password=request.POST['password'])
            login(request, authenticate(request, username=request.POST['name'],
                                        password=request.POST['password']))
            request.session.create()
            # request.session['id'] = user.id
            return redirect('/')
    return redirect('/signup/')

def content(request):
    # if (request.user.id == request.session['id']):
    context = {
        'user': request.user
    }
    return render(request, 'content.html', context)


# def add_user(request):
#     Custom_User.objects.create_user(
#         request.POST['name'], password=request.POST['password'])
#     return redirect('/')
