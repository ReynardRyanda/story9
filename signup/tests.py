from django.test import TestCase
from django.urls import resolve
from .views import *
# from django.contrib.auth.models import User

# Create your tests here.


class SignUpPageUnitTest(TestCase):

    def test_signup_page_url_exist(self):
        response = self.client.get('/signup/')
        self.assertEqual(200, response.status_code)

    def test_signup_page_template_used(self):
        response = self.client.get('/signup/')
        self.assertTemplateUsed(response, 'signup.html')

    def test_signup_function_used(self):
        response = resolve('/signup/')
        self.assertEqual(signup, response.func)

    def test_sign_up_user_is_submited(self):
        response = self.client.post('/add_user/', data={
            'name': 'Reynard',
            'password': 'ppwkeren'
        })
        # login = self.client.login(username='Reynard', password='ppwkeren')
        self.assertTrue(login)

    def test_password_length_too_short(self):
        response = self.client.post('/add_user/', data={
            'name': 'Reynard',
            'password': 'hai'
        })
        total_user = User.objects.all().count()
        self.assertEqual(0, total_user)

    def test_password_length_too_long(self):
        response = self.client.post('/add_user/', data={
            'name': 'Reynard',
            'password': 'a1234567890123456789012345678901234567890'
        })
        total_user = User.objects.all().count()
        self.assertEqual(0, total_user)

    def test_if_sign_up_with_same_username(self):
        response_1 = self.client.post('/add_user/', data={
            'name': 'Reynard',
            'password': 'ppwkeren'
        })
        response_2 = self.client.post('/add_user/', data={
            'name': 'Reynard',
            'password': 'gwkerenbngt'
        })
        total_user = User.objects.all().count()
        self.assertEqual(1, total_user)

    def test_content_page_url_exist(self):
        response = self.client.get('/')
        self.assertEqual(200, response.status_code)

    def test_content_page_template_used(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'content.html')

    def test_content_function_used(self):
        response = resolve('/')
        self.assertEqual(content, response.func)

    # def test_user_is_submitted(self):
    #     response = self.client.post('/add_user/', data={
    #         'name': 'ReynardRyanda',
    #         'password': 'ppwkeren'
    #     })
    #     self.assertEqual(302, response.status_code)
    #     jumlah_user = Custom_User.objects.all().count()
    #     self.assertEqual(1, jumlah_user)

    # def test_user_submitted_correctly(self):
    #     response = self.client.post('/add_user/', data={
    #         'name': 'ReynardRyanda',
    #         'password': 'ppwkeren',
    #     })
    #     user = Custom_User.objects.get(username='ReynardRyanda')
    #     self.assertEqual('ppwkeren', user.password)
